package handlers

import (
	"net/http"
	"time"

	"github.com/goleh/cache"

	"bitbucket.org/oleh-bedrenko/joinstuff/conf"
	"bitbucket.org/oleh-bedrenko/joinstuff/conf/msg"
	"bitbucket.org/oleh-bedrenko/joinstuff/models"
)

func RawAdd(w http.ResponseWriter, r *http.Request) (err error) {

	// Get channel ID:
	chID, usrErrMsg := models.GetChannelID(r)
	if usrErrMsg != "" {
		return models.SendError(w, r, usrErrMsg)
	}

	// Check parameters:
	params := models.GetRawData(r)
	if len(params) == 0 {
		return models.SendError(w, r, msg.NO_PARAMS)
	}

	// Setup DB:
	db := models.NewDB()
	defer db.Close()
	dbChannels := db.Channels()

	// Check channel secret key:
	usrErrMsg, err = models.CheckSecretKey(r, dbChannels, chID, models.WRITE)
	if err != nil {
		return err
	}
	if usrErrMsg != "" {
		return models.SendError(w, r, usrErrMsg)
	}

	// Writing raw data:
	if err = models.AddData(dbChannels, chID, models.IP(r), params); err != nil {
		return err
	}

	// Responding:
	return (&models.Response{Status: conf.RESP_STAT_OK}).Send(w, r)
}

func RawGet(w http.ResponseWriter, r *http.Request) (err error) {

	// Get channel number:
	chID, usrErrMsg := models.GetChannelID(r)
	if usrErrMsg != "" {
		return models.SendError(w, r, usrErrMsg)
	}

	// Setup DB:
	db := models.NewDB()
	defer db.Close()
	dbChannels := db.Channels()

	// Check channel secret key:
	usrErrMsg, err = models.CheckSecretKey(r, dbChannels, chID, models.READ)
	if err != nil {
		return err
	}
	if usrErrMsg != "" {
		return models.SendError(w, r, usrErrMsg)
	}

	// Get data key:
	dk := models.GetDataKey(r)

	// Check in cache:
	cacheKey := chID.Hex() + dk
	var dataCachedTime time.Time
	foundInCache, err := cache.Get(conf.CACHE_RAW_GET_ISSUED, cacheKey, &dataCachedTime)
	if err != nil {
		return err
	}

	// If raw data found in cache and they're not expired get'em:
	isInCache := false
	var data []models.DataItem
	if foundInCache && dataCachedTime.Add(conf.RAW_GET_CACHE_LIFE).Unix() > time.Now().Unix() {
		isInCache, _ = cache.Get(conf.CACHE_RAW_GET, cacheKey, &data)
	}

	// If not found in cache get from DB:
	if !isInCache {

		data, err = models.GetData(dbChannels, chID, dk)
		if err != nil {
			return err
		}

		// Save to cache:
		cache.Set(conf.CACHE_RAW_GET, cacheKey, data)
		cache.Set(conf.CACHE_RAW_GET_ISSUED, cacheKey, time.Now())
	}

	// Responding:
	resp := &models.Response{Status: conf.RESP_STAT_OK, Data: data}
	if isInCache {
		resp.CacheTime = dataCachedTime.Unix()
	}
	return resp.Send(w, r)
}
