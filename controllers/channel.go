package handlers

import (
	"log"
	"net/http"

	"bitbucket.org/oleh-bedrenko/joinstuff/conf"
	"bitbucket.org/oleh-bedrenko/joinstuff/conf/msg"
	"bitbucket.org/oleh-bedrenko/joinstuff/models"
)

func ChannelAdd(w http.ResponseWriter, r *http.Request) (err error) {

	// Check the token:
	u, usrErrMsg, err := models.GetAndCheckToken(r)
	if err != nil {
		return err
	}
	if usrErrMsg != "" {
		return models.SendError(w, r, usrErrMsg)
	}

	// Get channel data:
	ch := models.GetChannel(r)

	// Generate secret keys for channel read/writing:
	ch.GenerateSecretKeys()

	// Setup DB:
	db := models.NewDB()
	defer db.Close()
	dbChannels := db.Channels()

	// Add channel
	if err = ch.Add(dbChannels, u.ID); err != nil {
		return err
	}

	// Log message:
	log.Print("Dude registered new channel:", ch.ID)

	// Responding:
	return (&models.Response{Status: conf.RESP_STAT_OK, Data: ch}).Send(w, r)
}

func ChannelGet(w http.ResponseWriter, r *http.Request) (err error) {

	// Check the token:
	u, usrErrMsg, err := models.GetAndCheckToken(r)
	if err != nil {
		return err
	}
	if usrErrMsg != "" {
		return models.SendError(w, r, usrErrMsg)
	}

	// Get channel ID:
	chID, usrErrMsg := models.GetChannelID(r)
	if usrErrMsg != "" {
		return models.SendError(w, r, usrErrMsg)
	}

	// Setup DB:
	db := models.NewDB()
	defer db.Close()
	dbChannels := db.Channels()

	// Get channel(s) data
	var data interface{}
	if chID == "" {
		data, err = models.GetAllChannels(dbChannels, u.ID)
	} else {
		data, err = models.GetChannelByID(dbChannels, u.ID, chID)
	}
	if err != nil {
		return err
	}
	if chID != "" && data.(*models.Channel) == nil {
		models.SendError(w, r, msg.WRONG_CH)
	}

	// Responding:
	return (&models.Response{Status: conf.RESP_STAT_OK, Data: data}).Send(w, r)
}
