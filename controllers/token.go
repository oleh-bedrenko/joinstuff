package handlers

import (
	"log"
	"net/http"
	"time"

	"bitbucket.org/oleh-bedrenko/joinstuff/conf"
	"bitbucket.org/oleh-bedrenko/joinstuff/conf/msg"
	"bitbucket.org/oleh-bedrenko/joinstuff/models"
)

func TokenGet(w http.ResponseWriter, r *http.Request) (err error) {

	// Get data for auth:
	userEmail, inputToken, inputPass, userIP := models.GetLoginData(r)
	if userEmail == "" {
		return models.SendError(w, r, msg.AUTH_INVALID_EMAIL)
	}

	u := &models.User{Email: userEmail, IP: userIP}

	if inputToken != "" { // Auth by token

		// Check the token:
		var userErrMsg string
		userErrMsg, err = models.CheckToken(u, inputToken)
		if err != nil {
			return err
		}
		if userErrMsg != "" {
			return models.SendError(w, r, userErrMsg)
		}
		if u.Issued.Add(conf.TOKEN_LIFE_MIN).Unix() > time.Now().Unix() {
			return models.SendError(w, r, msg.AUTH_TOKEN_FRESH)
		}

	} else if inputPass != "" { // Auth by password

		// Setup DB:
		db := models.NewDB()
		defer db.Close()
		dbUsers := db.Users()

		// Get user if email and password are correct:
		err = models.GetUser(dbUsers, u, inputPass)
		if err != nil {
			return err
		}
		if u.ID == "" {
			return models.SendError(w, r, msg.AUTH_WRONG_PASS)
		}

		// Log message:
		log.Printf("Dude logged in. Email: %v, IP: %v.", userEmail, userIP)

		// Neither password nor token is set:
	} else {
		return models.SendError(w, r, msg.AUTH_TOKEN_OR_PASS)
	}

	// Make new token (login):
	if err = models.NewToken(u); err != nil {
		return nil
	}

	// Responding:
	return (&models.Response{Status: conf.RESP_STAT_OK, Token: u.Token}).Send(w, r)
}

func TokenDel(w http.ResponseWriter, r *http.Request) (err error) {

	// Check token:
	u, usrErrMsg, err := models.GetAndCheckToken(r)
	if err != nil {
		return err
	}
	if usrErrMsg != "" {
		return models.SendError(w, r, usrErrMsg)
	}

	// Delete the token:
	if err = models.DelToken(u.Email); err != nil {
		return err
	}

	// Responding:
	return (&models.Response{Status: conf.RESP_STAT_OK}).Send(w, r)
}
