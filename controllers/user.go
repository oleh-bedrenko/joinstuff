package handlers

import (
	"log"
	"net/http"

	"bitbucket.org/oleh-bedrenko/joinstuff/conf"
	"bitbucket.org/oleh-bedrenko/joinstuff/conf/msg"
	"bitbucket.org/oleh-bedrenko/joinstuff/models"
)

func UserAdd(w http.ResponseWriter, r *http.Request) (err error) {

	// Get email, password, ip:
	userEmail, _, userPass, userIP := models.GetLoginData(r)

	// Validation:
	if isValid, msg := models.ValidateUser(userEmail, userPass); !isValid {
		return models.SendError(w, r, msg)
	}

	// Setup DB:
	db := models.NewDB()
	defer db.Close()
	dbUsers := db.Users()

	// Check if email already exists:
	exists, err := models.UserExists(dbUsers, userEmail)
	if err != nil {
		return err
	}
	if exists {
		return models.SendError(w, r, msg.AUTH_EMAIL_EXISTS)
	}

	// Add user:
	id, err := models.AddUser(dbUsers, userEmail, userPass)
	if err != nil {
		return err
	}

	// Make new token (login):
	u := &models.User{Email: userEmail, IP: userIP, ID: id}
	if err = models.NewToken(u); err != nil {
		return err
	}

	// Log message:
	log.Printf("Dude signed up. Email: %v, IP: %v.", userEmail, userIP)

	// Responding:
	return (&models.Response{Status: conf.RESP_STAT_OK, Token: u.Token}).Send(w, r)
}
