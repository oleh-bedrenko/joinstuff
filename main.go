package main

import (
	"log"
	"net/http"

	"github.com/goleh/cache"

	"bitbucket.org/oleh-bedrenko/joinstuff/conf"
	"bitbucket.org/oleh-bedrenko/joinstuff/conf/msg"
	"bitbucket.org/oleh-bedrenko/joinstuff/controllers"
	"bitbucket.org/oleh-bedrenko/joinstuff/models"
)

func main() {

	// Setup memcached:
	cache.Init(conf.MEMCACHE_SERVER)
	cache.Flush()

	// Setup DB:
	if err := models.InitDB(conf.DB_SOURCE); err != nil {
		log.Panic(err)
	}
	models.EnsureDBIndexes()
	defer models.CloseDB()

	http.HandleFunc("/ch/add", HandlerWrapper(handlers.ChannelAdd))
	http.HandleFunc("/ch/get", HandlerWrapper(handlers.ChannelGet))
	http.HandleFunc("/add", HandlerWrapper(handlers.RawAdd))
	http.HandleFunc("/get", HandlerWrapper(handlers.RawGet))
	http.HandleFunc("/token/del", HandlerWrapper(handlers.TokenDel))
	http.HandleFunc("/token/get", HandlerWrapper(handlers.TokenGet))
	http.HandleFunc("/user/add", HandlerWrapper(handlers.UserAdd))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, conf.HELP_URL, http.StatusMovedPermanently)
	})

	log.Print("Listening port " + conf.LISTEN_PORT)
	panic(http.ListenAndServe(":"+conf.LISTEN_PORT, nil))
}

func HandlerWrapper(handler func(http.ResponseWriter, *http.Request) error) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		models.SendHeaders(w, r)
		if err := handler(w, r); err != nil {
			models.SendError(w, r, msg.INTERNAL_ERROR)
			log.Print("Error: " + err.Error())
		}
	}
}
