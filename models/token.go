package models

import (
	"crypto/rand"
	"encoding/hex"
	"time"

	"github.com/goleh/cache"

	"bitbucket.org/oleh-bedrenko/joinstuff/conf"
	"bitbucket.org/oleh-bedrenko/joinstuff/conf/msg"
)

func NewToken(u *User) error {
	b := make([]byte, conf.TOKEN_LEN/2)
	rand.Read(b)
	u.Token = hex.EncodeToString(b[:])
	u.Issued = time.Now()
	return cache.Set(conf.CACHE_USER, u.Email, u)
}

func CheckToken(u *User, token string) (string, error) {
	ip := u.IP
	found, err := cache.Get(conf.CACHE_USER, u.Email, u)
	if err != nil {
		return "", err
	}
	if !found || u.Token != token {
		return msg.AUTH_INVALID_TOKEN, nil
	}
	if u.Issued.Add(conf.TOKEN_LIFE_MAX).Unix() < time.Now().Unix() {
		return msg.AUTH_TOKEN_EXPIRED, nil
	}
	if u.IP != ip {
		return msg.AUTH_WRONG_IP, nil
	}
	return "", nil
}

func DelToken(email string) error {
	return cache.Del(conf.CACHE_USER, email)
}
