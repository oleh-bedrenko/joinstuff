package models

import (
	"net/http"
	"net/url"
	"strings"

	"github.com/goleh/cache"

	"bitbucket.org/oleh-bedrenko/joinstuff/conf"
	"bitbucket.org/oleh-bedrenko/joinstuff/conf/msg"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	EMAIL          = "email"
	TOKEN          = "token"
	PASSWORD       = "password"
	CHANNEL_NUMBER = "ch"
	SECRET_KEY     = "sk"
	DATA_KEY       = "dk"
	TITLE          = "title"
	DESCRIPTION    = "description"
	IS_PUB_WRITE   = "is_public_write"
	IS_PUB_READ    = "is_public_read"
	TRUE_VALUE     = "1"
)

func GetAndCheckToken(r *http.Request) (*User, string, error) {
	inputEmail := r.FormValue(EMAIL)
	if inputEmail == "" {
		return nil, msg.AUTH_INVALID_EMAIL, nil
	}
	inputToken := r.FormValue(TOKEN)
	if inputToken == "" {
		return nil, msg.AUTH_INVALID_TOKEN, nil
	}
	u := &User{Email: inputEmail, IP: IP(r)}
	if usrErrMsg, err := CheckToken(u, inputToken); usrErrMsg != "" || err != nil {
		return nil, usrErrMsg, err
	}
	return u, "", nil
}

func CheckSecretKey(r *http.Request, c *mgo.Collection, chID bson.ObjectId, direction ChannelDirection) (string, error) {
	SKeyUser := r.FormValue(SECRET_KEY)
	var SKeyReal, mainCacheKey string
	if direction == READ {
		mainCacheKey = conf.CACHE_CHANNEL_SK_READ
	} else {
		mainCacheKey = conf.CACHE_CHANNEL_SK_WRITE
	}
	secondaryCacheKey := chID.Hex()
	foundInCache, err := cache.Get(mainCacheKey, secondaryCacheKey, &SKeyReal)
	if err != nil {
		return "", err
	}

	// If not found in cache, get from DB:
	if !foundInCache {

		// Get secret keys from DB:
		chExists, access, err := GetChannelSecretKeys(c, chID)
		if err != nil {
			return "", err
		}
		if !chExists {
			return msg.WRONG_CH, nil
		}

		// Set secret keys in cache:
		cache.Set(conf.CACHE_CHANNEL_SK_READ, secondaryCacheKey, access.Read.Key)
		cache.Set(conf.CACHE_CHANNEL_SK_WRITE, secondaryCacheKey, access.Write.Key)

		if direction == READ {
			SKeyReal = access.Read.Key
		} else {
			SKeyReal = access.Write.Key
		}
	}

	// Check SK for:
	if SKeyReal != conf.CACHE_CHANNEL_SK_PUBLIC && SKeyReal != SKeyUser {
		return msg.WRONG_SK, nil
	}
	return "", nil
}

func GetLoginData(r *http.Request) (userEmail, userToken, userPass, userIP string) {
	userEmail = r.FormValue(EMAIL)
	userToken = r.FormValue(TOKEN)
	userPass = r.FormValue(PASSWORD)
	userIP = IP(r)
	return
}

func GetChannelID(r *http.Request) (bson.ObjectId, string) {
	ch := r.FormValue(CHANNEL_NUMBER)
	if ch == "" {
		return "", ""
	}
	if len(ch) != conf.CHANNEL_ID_LEN {
		return "", msg.WRONG_CH
	}
	return bson.ObjectIdHex(ch), ""
}

func GetRawData(r *http.Request) url.Values {
	u, _ := url.Parse(strings.Replace(r.URL.String(), ".", "", -1))
	params := u.Query()
	delete(params, CHANNEL_NUMBER)
	delete(params, SECRET_KEY)
	return params
}

func GetChannel(r *http.Request) *Channel {
	ch := &Channel{}
	ch.Info.Title = r.FormValue(TITLE)
	ch.Info.Description = r.FormValue(DESCRIPTION)
	ch.Access.Write.Public = r.FormValue(IS_PUB_WRITE) == TRUE_VALUE
	ch.Access.Read.Public = r.FormValue(IS_PUB_READ) == TRUE_VALUE
	return ch
}

func GetDataKey(r *http.Request) string {
	return r.FormValue(DATA_KEY)
}

func IP(r *http.Request) string {
	return strings.Split(r.RemoteAddr, ":")[0]
}
