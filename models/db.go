package models

import (
	"gopkg.in/mgo.v2"
)

var (
	DBSession *mgo.Session
)

type DB struct {
	*mgo.Session
}

func InitDB(source string) error {
	var err error
	DBSession, err = mgo.Dial(source)
	return err
}

func CloseDB() {
	DBSession.Close()
}

func NewDB() *DB {
	return &DB{Session: DBSession.Copy()}
}

func (db *DB) Users() *mgo.Collection {
	return DBSession.DB("joinstuff").C("users")
}

func (db *DB) Channels() *mgo.Collection {
	return DBSession.DB("joinstuff").C("channels")
}

func EnsureDBIndexes() {
	db := NewDB()
	defer db.Close()
	c := db.Channels()
	//@todo unique custom channel name
	c.EnsureIndexKey("owner_id", "status")
	c = db.Users()
	c.EnsureIndex(mgo.Index{Key: []string{"email"}, Unique: true})
	//c.EnsureIndexKey("email", "password", "status")
}
