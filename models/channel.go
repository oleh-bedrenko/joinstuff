package models

import (
	"crypto/rand"
	"encoding/hex"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/oleh-bedrenko/joinstuff/conf"
)

const (
	PUBLIC_SK = "-"
	READ      = true
	WRITE     = false
)

type ChannelDirection bool // read = true, write = false

type Channel struct {
	ID      bson.ObjectId `json:"id" bson:"_id,omitempty"`
	OwnerID bson.ObjectId `json:"-"  bson:"owner_id"`
	Info    struct {
		Title       string `json:"title"       bson:"title"`
		Description string `json:"description" bson:"description"`
	} `json:"info" bson:"info"`
	Access Access    `json:"access,omitempty" bson:"access,omitempty"`
	Status byte      `json:"-"                bson:"status"`
	Time   time.Time `json:"crtime"           bson:"crtime"`
}

type Access struct {
	Read struct {
		Public bool   `json:"is_public"    bson:"is_public"`
		Key    string `json:"sk,omitempty" bson:"sk,omitempty"`
	} `json:"read" bson:"read"`
	Write struct {
		Public bool   `json:"is_public"    bson:"is_public"`
		Key    string `json:"sk,omitempty" bson:"sk,omitempty"`
	} `json:"write" bson:"write"`
}

func (ch *Channel) GenerateSecretKeys() {
	b := make([]byte, conf.SK_LEN/2)
	rand.Read(b)
	ch.Access.Read.Key = hex.EncodeToString(b[:])
	rand.Read(b)
	ch.Access.Write.Key = hex.EncodeToString(b[:])
}

func (ch *Channel) Add(c *mgo.Collection, userID bson.ObjectId) error {
	ch.ID = bson.NewObjectId()
	ch.OwnerID = userID
	ch.Status = conf.STATUS_ACTIVE
	ch.Time = time.Now()
	err := c.Insert(ch)
	if err != nil {
		ch = nil
	}
	hideKeysIfPublic(ch)
	return err
}

func GetChannelByID(c *mgo.Collection, userID, channelID bson.ObjectId) (*Channel, error) {
	ch := new(Channel)
	err := c.Find(bson.M{
		"_id":      channelID,
		"owner_id": userID,
		"status":   conf.STATUS_ACTIVE,
	}).One(ch)
	if err == mgo.ErrNotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	hideKeysIfPublic(ch)
	return ch, nil
}

func GetAllChannels(c *mgo.Collection, userID bson.ObjectId) ([]Channel, error) {
	iter := c.Find(bson.M{
		"owner_id": userID,
		"status":   conf.STATUS_ACTIVE,
	}).Iter()
	var chs []Channel
	ch := new(Channel)
	for iter.Next(ch) {
		hideKeysIfPublic(ch)
		chs = append(chs, *ch)
	}
	iter.Close()
	return chs, iter.Err()
}

func hideKeysIfPublic(ch *Channel) {
	if ch.Access.Read.Public {
		ch.Access.Read.Key = ""
	}
	if ch.Access.Write.Public {
		ch.Access.Write.Key = ""
	}
}

func GetChannelSecretKeys(c *mgo.Collection, chID bson.ObjectId) (bool, *Access, error) {
	ch := &struct {
		Access Access
	}{}
	err := c.FindId(chID).One(ch)
	if err == mgo.ErrNotFound {
		return false, nil, nil
	}
	if err != nil {
		return false, nil, err
	}
	if ch.Access.Read.Public {
		ch.Access.Read.Key = PUBLIC_SK
	}
	if ch.Access.Write.Public {
		ch.Access.Write.Key = PUBLIC_SK
	}
	return true, &ch.Access, nil
}
