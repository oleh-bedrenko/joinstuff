package models

import (
	"compress/gzip"
	"encoding/json"
	"net/http"
	"strings"

	"bitbucket.org/oleh-bedrenko/joinstuff/conf"
)

type Response struct {
	Status    string      `json:"status"`
	Msg       string      `json:"msg,omitempty"`
	Data      interface{} `json:"data,omitempty"`
	Token     string      `json:"token,omitempty"`
	CacheTime int64       `json:"cache_time,omitempty"`
}

func (resp *Response) Send(w http.ResponseWriter, r *http.Request) error {
	if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
		return json.NewEncoder(w).Encode(resp)
	}
	w.Header().Set("Content-Encoding", "gzip")
	gz := gzip.NewWriter(w)
	err := json.NewEncoder(gz).Encode(resp)
	gz.Close()
	return err
}

func SendError(w http.ResponseWriter, r *http.Request, userErrMsg string) error {
	return (&Response{Status: conf.RESP_STAT_FAIL, Msg: userErrMsg}).Send(w, r)
}

func SendHeaders(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT")
	w.Header().Set("Access-Control-Allow-Headers", r.Header.Get("Access-Control-Request-Headers"))
	w.Header().Set("Content-Type", "application/json")
}
