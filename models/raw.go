package models

import (
	"net/url"
	"time"

	"bitbucket.org/oleh-bedrenko/joinstuff/conf"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type DataItem struct {
	Data map[string]string `json:"data" bson:"data,omitempty"`
	Time time.Time         `json:"time" bson:"time,omitempty"`
	IP   string            `json:"-"    bson:"ip,omitempty"`
}

func GetData(c *mgo.Collection, chID bson.ObjectId, dk string) ([]DataItem, error) {
	if dk != "" {
		dk = "." + dk
	}
	pipe := c.Pipe([]bson.M{
		{"$match": bson.M{"_id": chID, "status": conf.STATUS_ACTIVE}},
		{"$unwind": "$raw"},
		{"$match": bson.M{"raw.data" + dk: bson.M{"$exists": true}}},
		{"$project": bson.M{"_id": 0, "time": "$raw.time", "data" + dk: "$raw.data" + dk}},
	})
	var res []DataItem
	if err := pipe.All(&res); err != nil {
		if err == mgo.ErrNotFound {
			return nil, nil
		}
		return nil, err
	}
	return res, nil
}

func AddData(c *mgo.Collection, chID bson.ObjectId, ip string, params url.Values) error {
	// Prepare parameters:
	di := DataItem{Time: time.Now(), IP: ip, Data: make(map[string]string)}
	for key, value := range params {
		if len(value) != 0 {
			di.Data[key] = value[0]
		}
	}
	return c.UpdateId(chID, bson.M{
		"$push": bson.M{
			"raw": bson.M{
				"$each":  []DataItem{di},
				"$sort":  bson.M{"time": 1},
				"$slice": 100000,
			},
		},
	})
}

/*
MongoDB queries:

Get by data key:
db.channels.aggregate([
	{$match: {"_id": ..., "status": ....}},
	{$unwind: "$raw"},
	{$match: {"raw.data.x": {$exists: 1}}},
	{$project: {"time": "$raw.time", "data": "$raw.data.x", "_id": 0}}
])

Get all:
db.channels.aggregate([
	{$match: {"_id": ..., "status": ...}},
	{$unwind: "$raw"},
	{$project: {"time": "$raw.time", "data": "$raw.data", "_id": 0}}
])
*/
