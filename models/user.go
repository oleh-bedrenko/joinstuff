package models

import (
	"crypto/sha1"
	"encoding/hex"
	"regexp"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"bitbucket.org/oleh-bedrenko/joinstuff/conf"
	"bitbucket.org/oleh-bedrenko/joinstuff/conf/msg"
)

const (
	SALT_PREFIX  = "*f"
	SALT_POSTFIX = "ж_"
)

type User struct {
	ID     bson.ObjectId
	Token  string
	Issued time.Time
	IP     string
	Email  string
	Data   map[string]string
}

func AddUser(c *mgo.Collection, email, password string) (bson.ObjectId, error) {
	password = PrepUserPass(password)
	id := bson.NewObjectId()
	err := c.Insert(bson.M{
		"_id":      id,
		"email":    email,
		"password": password,
		"settings": bson.M{"time_zone": 0},
		"status":   conf.STATUS_ACTIVE,
		"cttime":   time.Now(),
	})
	return id, err
}

func GetUser(c *mgo.Collection, u *User, password string) error {
	usr := &struct {
		ID   bson.ObjectId `bson:"_id"`
		Data map[string]string
	}{}
	err := c.Find(bson.M{
		"email":    u.Email,
		"password": PrepUserPass(password),
		"status":   conf.STATUS_ACTIVE,
	}).One(usr)
	if err == nil {
		u.ID = usr.ID
		u.Data = usr.Data
		return nil
	}
	if err == mgo.ErrNotFound {
		return nil
	}
	return err
}

func UserExists(c *mgo.Collection, email string) (bool, error) {
	n, err := c.Find(bson.M{"email": email}).Count()
	return n != 0, err
}

func ValidateUser(email, password string) (bool, string) {
	re := regexp.MustCompile(conf.VALID_EMAIL_REGEX)
	matched := re.Match([]byte(email))
	if !matched {
		return false, msg.AUTH_INVALID_EMAIL
	}
	if len(password) < conf.VALID_PASS_MIN_LEN {
		return false, msg.AUTH_INVALID_PASS
	}
	return true, ""
}

func PrepUserPass(password string) string {
	hash := sha1.Sum([]byte(SALT_PREFIX + password + SALT_POSTFIX))
	return hex.EncodeToString(hash[:])
}
