package conf

import "time"

const (
	LISTEN_PORT     = "8080"
	MEMCACHE_SERVER = "localhost:11211"
	HELP_URL        = "http://joinx.cf/#/help"

	RESP_STAT_OK   = "OK"
	RESP_STAT_FAIL = "FAIL"

	VALID_PASS_MIN_LEN = 6
	VALID_EMAIL_REGEX  = ".+@.+\\..+"
	RAW_GET_CACHE_LIFE = 15 * time.Second

	SK_LEN         = 24
	TOKEN_LEN      = 24
	CHANNEL_ID_LEN = 24

	TOKEN_LIFE_MAX = 45 * time.Minute
	TOKEN_LIFE_MIN = 5 * time.Minute

	DB_SOURCE = "localhost"

	STATUS_ACTIVE  = 1
	STATUS_BLOCKED = 2
	STATUS_REMOVED = 3

	CACHE_USER              = "u"
	CACHE_RAW_GET           = "rg"
	CACHE_RAW_GET_ISSUED    = "rgi"
	CACHE_CHANNEL_SK_READ   = "csr"
	CACHE_CHANNEL_SK_WRITE  = "csw"
	CACHE_CHANNEL_SK_PUBLIC = "-"
)
